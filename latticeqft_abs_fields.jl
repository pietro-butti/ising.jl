#---------------------------------------------------------------------
abstract type AbstractField{T} end

struct Field{T} <: AbstractField{T}
    dof::T
end


# Overload * (group operation)
@eval function Base.:*(a::AbstractField{T},b::AbstractField{T}) where T 
    return Field{T}(Base.:*(a.dof,b.dof))
end

# Overload other algebra operation (probably outside the group)
for op in (:+, :-)
    @eval function Base.$op(a::AbstractField{T},b::AbstractField{T}) where T
        try
            return Base.$op(a.dof,b.dof)
        catch
            error("TypeError: $(Base.$op) not defined between $T variable types.")
        end
    end
end

# Overload scalar operation (returning Float64)
for op in (:+, :-, :*, :/, :^)
    @eval function Base.$op(a::AbstractField{T},b::Number) where T
        try
            return Base.$op(a.dof,b)
        catch
            error("TypeError: $(Base.$op) not defined between $T variable types and `Numbers`.")
        end
    end
    @eval function Base.$op(a::Number,b::AbstractField{T}) where T
        try
            return Base.$op(b,a)
        catch
            error("TypeError: $(Base.$op) not defined between $T variable types and `Numbers`.")
        end
    end
end

# Overload show function for different cases
function Base.show(io::IO, a::AbstractField{Bool})
    a.dof ? print(io,"↑") : print(io,"↓")
    return
end
function Base.show(io::IO, a::AbstractField{T}) where T<:Number
    print(io,a.dof)
    return
end
#---------------------------------------------------------------------




# ## ################################# EXAMPLE ########################################

# # NOT WORKING
# primitive type Spin <: Integer 8 end

# Spin(tf::Bool) = tf::Spin
# Spin(i::Int) = Bool(i)::Spin


# function Base.:*(a::Spin,b::Spin)



## ---------------------

struct Spin
    spin::Bool
end

function Base.show(io::IO, a::Spin)
    a.spin ? print(io,"↑") : print(io,"↓")
    return
end

Base.:*(s1::Spin,s2::Spin)::Spin     = s1.spin||s2.spin ? Spin(s1.spin&&s2.spin) : Spin(true)
Base.:*(s1::Spin,r::Number)::Float64 = Float64(r*(-1)^s1.spin)
Base.:*(r::Number,s2::Spin)::Float64 = s2*r
Base.:+(s1::Spin,s2::Spin)::Float64  = Float64((-1)^s1.spin + (-1)^s2.spin)