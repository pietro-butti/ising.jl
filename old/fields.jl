using CircularArrays

abstract type AbstractFieldType end 

struct Spin # <: AbstractFieldType
    s::Bool
end    

# Overload addition and multiplication
for op in (:+, :-, :*, :/, :^)
    @eval function Base.$op(a::Spin,b::Spin)
        return Base.$op(a.spin,b.spin)
    end

    @eval function Base.$op(a::Spin,b::Number)
        return Base.$op(a.spin,b)
    end

    @eval function Base.$op(a::Number,b::Spin)
        return Base.$op(a,b.spin)
    end
end


# Overload random scalar function 
function Base.rand(Spin)
    return rand(Bool)
end

# Overload neutral element function
