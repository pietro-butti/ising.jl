using CircularArrays

struct Parameters
    L::Vector{Int64}
    couplings::Vector{Float64}
end
mutable struct ScalarField{FieldType} #<: AbstractFieldType}
    pars::Parameters
    field::CircularArray{FieldType}
    
    function ScalarField{FieldType}(p) where FieldType 
        this = new()
        this.pars = p
        aux = Array{FieldType}(undef,p.L...); this.field = CircularArray(aux)
        return this
    end
end

function initialize(field::ScalarField,flag::String) 
    for i in eachindex(field.field)
        if flag=="hot"
            field.field
        elseif flag=="cold"

        end
    end
end

function

function Base.show(io::IO, a::ScalarField)
    print(io,"ScalarField{$(eltype(a.field))} on a $(a.pars.L) lattice with following couplings: $(a.pars.couplings)")
    return
end


##

include("fields.jl")

par = Parameters([3,5,7,3],[0.5])
prova = ScalarField{Spin}(par)
initialize(prova)
