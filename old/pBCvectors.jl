"""
    PeriodicArray{T,N} <: AbstractArray{T,N}

`N`-dimensional array backed from an `AbstractArray{T, N}` of type `A` with fixed size and circular indexing.
"""
struct PeriodicArray{T, N, A <: AbstractArray{T,N} } <: AbstractArray{T,N}
    array::A
    PeriodicArray{T,N}(a::A) where A <: AbstractArray{T,N} where {T,N} = new{T,N,A}(a)
end
PeriodicArray(array::AbstractArray{T,N}) where {T,N} = PeriodicArray{T,N}(array)
PeriodicArray{T}(array::AbstractArray{T,N}) where {T,N} = PeriodicArray{T,N}(array)
PeriodicArray(el::T,size) where T = PeriodicArray(fill(el,size))


## https://github.com/Vexatos/CircularArrays.jl/blob/master/src/CircularArrays.jl

# Periodic BC indexing function
pBCindex(i::Int,N::Int) = 1 + mod(i-1,N)
pBCindex(I,N::Int) = [pBCindex(i,N)::Int for i in I]

# Overloading of basic vector functions
Base.size(v::pVector) = size(v.vector)

# Base.getindex(v::pVector,i::Int)         = getindex(v.vector, pBCindex(i,length(v.vector)))
# Base.getindex(v::pVector,I::Vararg{Int}) = getindex(v.vector, pBCindex(I,length(v.vector)))

# Base.setindex!(v::pVector, el, i::Int)         = setindex!(v.vector, el, pBCindex(i,length(v.vector)))
# Base.setindex!(v::pVector, el, I::Vararg{Int}) = setindex!(v.vector, el, pBCindex(I,length(v.vector))) 








# @inline pVector{T}(x::Tuple{Vararg{Any, N}}) where T = Vector{T}()
# @inline function (::Type{FA})(x::Tuple{Vararg{Any, N}}) where {N, FA <: FieldArray}
#     return FA(x...)
# end


