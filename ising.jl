using CircularArrays

## ======================================================================================
## ====================== DEGREES OF FREEDOM ============================================
## ======================================================================================

mutable struct Spin
    spin::Bool
end

Base.:*(s1::Spin,s2::Spin)::Spin     = s1.spin||s2.spin ? Spin(s1.spin&&s2.spin) : Spin(true)
# Base.:*(s1::Spin,s2::Spin)::Float64  = s1.spin||s2.spin ? (-1)^Spin(s1.spin&&s2.spin).spin : (-1)^Spin(true).spin
Base.:*(s1::Spin,r::Number)::Float64 = Float64(r*(-1)^s1.spin)
Base.:*(r::Number,s2::Spin)::Float64 = s2*r
Base.:+(s1::Spin,r::Number)::Float64 = Float64(r+(-1)^s1.spin)
Base.:+(r::Number,s2::Spin)::Float64 = s2+r
Base.:+(s1::Spin,s2::Spin)::Float64  = Float64((-1)^s1.spin + (-1)^s2.spin)
Base.:-(s1::Spin,r::Number)::Float64 = Float64(r-(-1)^s1.spin)
Base.:-(r::Number,s2::Spin)::Float64 = s2-r
Base.:-(s1::Spin,s2::Spin)::Float64  = Float64((-1)^s1.spin - (-1)^s2.spin)

flip(s::Spin) = Spin(!s.spin)
function flip!(s::Spin) 
    s.spin = !s.spin
    return
end

function Base.show(io::IO, a::Spin)
    # a.spin ? print(io,"↑") : print(io,"↓")
    a.spin ? print(io,".") : print(io,"O")
    return
end

## ======================================================================================
## ================================= SPIN FIELD =========================================
## ======================================================================================


struct Parameters
    L::Vector{Int64}
    couplings::Vector{Float64}
end
function Base.show(io::IO, a::Parameters)
    print(io,"L=$(a.L), couplings=$(a.couplings)")
    return
end
## ============================== SPIN FIELD METHODS ====================================


function sum_nneigh(L::Vector{Int64},ss::CircularArray{T}) where T
    H = 0.0
    for I in eachindex(ss)
        for dim in 1:length(L)
            Jp = [i for i in Tuple(I)]; Jp[dim]+=1; Jp = CartesianIndex(Tuple(i for i in Jp))
            s_sp1 = ss[I]*ss[Jp]
        
            H += s_sp1
        end
    end
    return H
end



mutable struct SpinField
    pars::Parameters
    σ::CircularArray{Spin}

    Ndim::Int64
    vol::Int64
    H::Float64

    function SpinField(pars::Parameters; start="hot", computeH=true) 
        this = new(pars)
        aux = Array{Spin}(undef,pars.L...)
        this.Ndim = length(pars.L)
        this.vol  = prod(pars.L)

        if start=="hot"
            for I in eachindex(aux)
                aux[I] = Spin(rand(Bool))
            end    
        elseif start=="cold"
            for I in eachindex(aux)
                aux[I] = Spin(0)
            end    
        end    
        this.σ = CircularArray(aux)

        computeH ? this.H = -pars.couplings[1]*sum_nneigh(this.pars.L,this.σ) - pars.couplings[2]*sum(this.σ) : 1
        
        return this
    end    
end    



sum_nneigh(ss::SpinField) = sum_nneigh(ss.pars.L,ss.σ)
sum_global(ss::SpinField) = sum(ss.σ)



function Hamiltonian(ss::SpinField,interactions::Vector{Function})  
    length(interactions)!=length(ss.pars.couplings) ? error("UserError: $(:interactions) has a different length from $(:ss.pars.couplings)") : 0
    H = 0.0
    for (f,g) in zip(interactions,ss.pars.couplings)
        H -= g*f(ss)
    end
    return H
end



function Base.show(io::IO, s::SpinField)
    print(io,"Spin field with $(s.pars). H=$(s.H)\n")
    
    ls = s.pars.L
    if length(ls)==2 
        for row in 1:ls[1]
            for col in 1:ls[2]
                print("$(s.σ[row,col]) ")
            end    
            print("\n")
        end    
        return
    end    
    return
end    

function behold(s::SpinField)
    ls = s.pars.L
    str = "\n "
    for row in 1:ls[1]
        aux = ""
        for col in 1:ls[2]
            aux *= "$(s.σ[row,col]) "
        end    
        aux *= "\n "
        str *= aux
    end    
    return str
end



## ======================================================================================
## ======================================================================================
## ======================================================================================


# This function calculate the difference in Hamiltonian by flipping one spin, given the coordinate
function δH(ss::SpinField,toflip::Vector{Int64})
    length(toflip)!=ss.Ndim ? error("$toflip is not a correct vector of coordinates dim(L)=$(ss.Ndim)") : 0

    I = CartesianIndex(Tuple(i for i in toflip))
    δs = flip(ss.σ[I]) - ss.σ[I]

    NN = 0.
    for dim in 1:length(toflip)
        Jp = deepcopy(toflip); Jm = deepcopy(toflip)
        Jp[dim] += 1; Jm[dim] -= 1
        Jp = CartesianIndex(Tuple(i for i in Jp)); Jm = CartesianIndex(Tuple(i for i in Jm))
        NN += ss.σ[Jp] + ss.σ[Jm]
    end    
    return -δs*(ss.pars.couplings[1]*NN + ss.pars.couplings[2])
end    

Metropolis(ΔH::Float64) = rand()≤exp(-ΔH) ? true : false

# This function does the accept-reject Metropolis step
function update!(ss::SpinField, Nupdates::Int64)
    proposal = deepcopy(ss)
    ΔH = 0.
    for _ in 1:Nupdates
        randI =  [rand(1:ss.pars.L[dim]) for dim in 1:ss.Ndim]
        ΔH += δH(proposal,randI)

        randI = CartesianIndex(Tuple(ii for ii in randI))
        flip!(proposal.σ[randI])
    end

    Metropolis(ΔH) ? ss.σ[:]=proposal.σ[:] : return 0
    return 1
end


## ==================================================================================================
## ==================================================================================================
## ==================================================================================================

using ProgressMeter

p = Parameters([3,3],[5.5,0.0])
ss = SpinField(p,start="hot")



##


##

# interactions = [
#     sum_nneigh,
#     sum_global
# ]

# MCsteps = 1000


# M = Vector{Float64}(undef,MCsteps)


# counter = 0
# @showprogress for MCtime in 1:MCsteps
#     global counter += update!(ss,5)

#     M[MCtime] = sum(ss.σ)/ss.vol
# end

# println(counter/MCsteps)

# ##


# using PyPlot

# plot(M)
# display(gcf())
# close()

# ##
































##

prog = Progress(MCsteps)
counter = 0
for mct in 1:MCsteps
    global counter += update!(ss,1)

    # ProgressMeter.next!(prog; showvalues = [("acc rate",Float64(counter)/Float64(MCsteps)),("H",round(Hamiltonian(ss,interactions),digits=4)) ])
    ProgressMeter.next!(prog; showvalues = [("counter",counter),("prova\n","$(behold(ss))")])
    sleep(0.051)
end



##